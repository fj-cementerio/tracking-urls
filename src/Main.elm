module Main exposing (Model, Msg(..), Site, SiteData, Status(..), getRandomCatGif, gifDecoder, init, main, subscriptions, update, view, viewGif)

import Browser
import Html exposing (..)
import Html.Attributes exposing (..)
import Html.Events exposing (..)
import Http as H exposing (..)
import Json.Decode exposing (Decoder, field, string)
import Task exposing (..)
import Time exposing (..)



-- MAIN


main =
    Browser.element
        { init = init
        , update = update
        , subscriptions = subscriptions
        , view = view
        }



-- MODEL


type alias SiteData =
    { at : Int
    , dur : Float
    , response : Maybe String
    , text : String
    }


type alias Site =
    { id : Int
    , url : String
    , apiKey : String
    , data : List SiteData
    }


type Status
    = Failure
    | Loading
    | Success String


type alias Model =
    { st : Status
    , urlDeLosGatos : Maybe String
    , sites : List Site
    }


init : () -> ( Model, Cmd Msg )
init _ =
    ( Model Loading Nothing [ Site 1 "https://httpbin.org/get" "mojarra" [] ], getRandomCatGif )



-- UPDATE


type Msg
    = MorePlease
    | GotGif (Result H.Error String)
    | Loaded Site (Result H.Error String)
    | TestLink Site
    | NewTime Int


update : Msg -> Model -> ( Model, Cmd Msg )
update msg model =
    case msg of
        MorePlease ->
            ( Model Loading model.urlDeLosGatos model.sites, getRandomCatGif )

        GotGif result ->
            case result of
                Ok url ->
                    ( Model (Success url) (Just url) model.sites, Cmd.none )

                Err _ ->
                    ( Model Failure model.urlDeLosGatos model.sites, Cmd.none )

        Loaded site result ->
            case result of
                Ok m ->
                    ( Model
                        (Maybe.withDefault Loading (Maybe.map Success model.urlDeLosGatos))
                        model.urlDeLosGatos
                        (findChange
                            (updateData
                                (SiteData 0 0 (Just m) m)
                                site
                            )
                            model.sites
                        )
                    , Cmd.none
                    )

                Err _ ->
                    ( Model (Maybe.withDefault Loading (Maybe.map Success model.urlDeLosGatos))
                        model.urlDeLosGatos
                        model.sites
                    , Cmd.none
                    )

        TestLink s ->
            ( Model Loading model.urlDeLosGatos model.sites, getSite s )

        NewTime _ ->
            ( model, Cmd.none )


findChange : Site -> List Site -> List Site
findChange s ls =
    s :: List.filter (\a -> a.id /= s.id) ls


updateData : SiteData -> Site -> Site
updateData sd s =
    { s | data = sd :: s.data }



-- SUBSCRIPTIONS


subscriptions : Model -> Sub Msg
subscriptions model =
    Sub.none



-- VIEW


view : Model -> Html Msg
view model =
    div []
        ([ h2 [] [ text "Random Cats" ]
         , viewGif model
         ]
            ++ Maybe.withDefault [] (Maybe.map getButton (List.head model.sites))
            ++ List.map printSite model.sites
        )


printSite : Site -> Html Msg
printSite s =
    Html.section
        [ class "section" ]
        [ div [ class "container" ]
            (div
                [ class "row border-top border-bottom" ]
                [ div [ class "col-2" ] [ strong [] [ text "Fecha" ] ]
                , div [ class "col-3" ] [ strong [] [ text "Duración" ] ]
                , div [ class "col-6" ] [ strong [] [ text "Texto" ] ]
                , div [ class "" ] [ strong [] [ text "Mensaje" ] ]
                ]
                :: List.map printResult s.data
            )
        ]


printResult : SiteData -> Html Msg
printResult d =
    div
        [ class "row" ]
        [ div [ class "col-2" ] [ d.at |> millisToPosix |> toUtcString |> text ]
        , div [ class "col-3" ] [ d.dur |> String.fromFloat |> text ]
        , div [ class "col-6" ] [ d.text |> text ]
        , div [ class "" ] [ Maybe.withDefault "" d.response |> text ]
        ]


getButton : Site -> List (Html Msg)
getButton s =
    [ h2 [] [ text "This is a test in the link" ]
    , button [ onClick (TestLink s) ] [ text "LINK!" ]
    ]


viewGif : Model -> Html Msg
viewGif model =
    case model.st of
        Failure ->
            div []
                [ text "I could not load a random cat for some reason. "
                , button [ onClick MorePlease ] [ text "Try Again!" ]
                ]

        Loading ->
            text "Loading..."

        Success url ->
            div []
                [ button [ onClick MorePlease, style "display" "block" ] [ text "More Please!" ]
                , img [ src url ] []
                ]



-- HTTP


getNewTime : Cmd Msg
getNewTime =
    Task.perform NewTime
        (Time.now
            |> andThen (\t -> succeed (Time.posixToMillis t))
        )


getSite : Site -> Cmd Msg
getSite a =
    H.request
        { method = "GET"
        , headers = [ H.header "Authorization" a.apiKey ]
        , url = a.url
        , body = H.emptyBody
        , expect = H.expectJson (Loaded a) headDecoder
        , timeout = Nothing
        , tracker = Nothing
        }


getRandomCatGif : Cmd Msg
getRandomCatGif =
    H.get
        { url = "https://api.giphy.com/v1/gifs/random?api_key=dc6zaTOxFJmzC&tag=cat"
        , expect = H.expectJson GotGif gifDecoder
        }


headDecoder : Decoder String
headDecoder =
    field "headers" (field "Authorization" string)


gifDecoder : Decoder String
gifDecoder =
    field "data" (field "image_url" string)


toUtcString : Time.Posix -> String
toUtcString time =
    String.fromInt (toHour utc time)
        ++ ":"
        ++ String.fromInt (toMinute utc time)
        ++ ":"
        ++ String.fromInt (toSecond utc time)
        ++ " (UTC)"
